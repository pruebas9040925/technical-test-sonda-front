run-local:
	docker-compose -f ./docker/docker-compose.local.yml --project-directory ./ -p front_test_app down && \
	docker-compose -f ./docker/docker-compose.local.yml --project-directory ./ -p front_test_app build --no-cache && \
	docker-compose -f ./docker/docker-compose.local.yml --project-directory ./ -p front_test_app up -d


rm-local:
	@docker-compose -f ./docker/docker-compose.local.yml --project-directory ./ -p front_test_app down -v --remove-orphans
	@docker network rm frontend-network || true
