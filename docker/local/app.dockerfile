# Stage 1: Base image
FROM node:14.16.0-alpine AS base

ARG VITE_APP_BASE_URL
ARG VITE_APP_ENV
ARG DOCKER_APP_PORT
ARG SERVICE_URL_PROD
ARG HEROKU_APP_NAME
ARG PORT
ARG DOCKER_IMAGE_APP

ENV VITE_APP_BASE_URL=$VITE_APP_BASE_URL
ENV VITE_APP_ENV=$VITE_APP_ENV
ENV DOCKER_APP_PORT=DOCKER_APP_PORT
ENV SERVICE_URL_PROD=SERVICE_URL_PROD
ENV HEROKU_APP_NAME=HEROKU_APP_NAME
ENV PORT=PORT
ENV DOCKER_IMAGE_APP=DOCKER_IMAGE_APP

WORKDIR /app

# Install dependencies
COPY package*.json ./
RUN npm install --silent

# Copy the entire project
COPY . .

# Stage 2: Development environment
FROM base AS local

CMD npm run dev
RUN apk add --no-cache xdg-utils


# Stage 3: Build
FROM base AS build

RUN npm run build


# Stage 4: Production server
FROM nginx:1.21.0-alpine AS server

COPY --from=build /app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY docker/local/ec2.nginx.conf /etc/nginx/conf.d

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]