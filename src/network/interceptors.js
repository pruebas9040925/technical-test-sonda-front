import axios from 'axios';
import cookieHandler from '../utils/helpers/cookie.handler';
import eventBus from '../utils/helpers/eventBus';


// import router from '../router'
const urlApi='https://pruebatecnica-84c857e0a204.herokuapp.com'

const authorization = http => ({
  authRequest: request => {
    const accessToken = cookieHandler().getAccessToken();
    if (accessToken) {
      request.headers = { Authorization: `Bearer ${accessToken}` };
    }
    return request;
  },

  authResponse: async error => {
    const { config, response } = error;
    const  loginURI=`${urlApi}/api/v1/login`;
    if (response.status === 401 && config.url !== loginURI) {
      if (!window.refreshing) {
        const { getRefreshToken, setTokens, removeToken } = cookieHandler();
        window.refreshing = true;
        try {
          const refresh = await http.post(`${urlApi}/api/v1/refresh-token`, {
            token: 'refresh_token',
            refreshToken: getRefreshToken(),
          });
          console.log('refreshh',refresh);
          if (refresh && (refresh.status === 201 || refresh.status==200) ) {
            setTokens(refresh.data);
            axios.defaults.headers.common['Authorization'] = ` Bearer ${refresh.data.access_token}`;
            window.refreshing = false;
            window.logout = false;
            eventBus.dispatch('refreshed');
            return http(config);
          }
          removeToken();
          throw new Error('unauthorized');
          
        } catch (e) {
          // removeToken();
          // router.push('/login');
          window.refreshing = false;
          window.logout = true;
          return Promise.reject(error);
        }
      }

      return new Promise(resolve => {
        eventBus.addEventListener('refreshed', () => {
          eventBus.remove('refreshed');
          if (window.logout) {
            return Promise.reject(error);
          }
          resolve(http(config));
        });
      });
    }
    return Promise.reject(error);
  }
});

export default authorization;