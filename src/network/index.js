import axios from "axios";

const httpConfig = {
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
};
const http = axios.create(httpConfig);


const urlApi='https://technicaltestsonda-291c1c8584be.herokuapp.com/';
const apidos ='jsonhttps://jsonplaceholder.typicode.com/'
const network = {};

network.users = () => http.get("json/users", { withCredentials: false });
network.userPosts =()=>http.get('json/posts',{ withCredentials: false });
network.postUser=(payload)=>http.post('json/users',payload,{withCredentials:false})

export default network;
