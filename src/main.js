import { createApp } from "vue";
import axios from "axios";
import App from "./App.vue";
import store from "./store";
import router from "./router/router";
import "./less/app.less";

import BootstrapVue3 from "bootstrap-vue-3";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue-3/dist/bootstrap-vue-3.css";

import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';


import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import {
  faUser,
  faCog,
  faHouse,
  faUsers,
  faSearch,
  faUserPlus,
  faInfoCircle,
} from "@fortawesome/free-solid-svg-icons";

import * as am5 from "@amcharts/amcharts5";
import * as am5percent from "@amcharts/amcharts5/percent";

library.add(
  faUser,
  faCog,
  faHouse,
  faUsers,
  faSearch,
  faUserPlus
);

const app = createApp(App);
app.config.globalProperties.am5 = am5;

app.component("font-awesome-icon", FontAwesomeIcon);

axios.defaults.withCredentials = true;
app.config.globalProperties.axios = axios;

app.use(store).use(router).use(BootstrapVue3).use(VueSweetalert2).mount("#app");
