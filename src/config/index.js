const config = () => ({
  appEnv: import.meta.env.VITE_APP_ENV || "production",
  baseURL: import.meta.env.VITE_APP_BASE_URL || "https://technicaltestsonda-291c1c8584be.herokuapp.com",
  timeout: 120000,
  prefix: import.meta.env.VITE_APP_SERVICE_URL || "http://localhost:        ",
});
export default config;
