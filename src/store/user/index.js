import network from "../../network";
import {
  FETCH_ALL_USERS,
  GET_USERS,
  SET_USERS,

  GET_USER_POSTS,
  SET_USER_POSTS,
  FETCH_USER_POSTS,
  POST_USER,
  SET_DELETE_USER,
  DELETE_USER,
  SET_FETCH_USERS_POSTS,
  GET_USER_POST_FILTER_BY_ID

} from "../types";

const store = {};
const getDefaultState = () => ({
  users:[],
  userPosts:[],
});

store.state = getDefaultState();

store.getters = {
  [GET_USERS](state){
    return state.users
  },
  [GET_USER_POSTS](state){
    return state.userPosts
  },
  [GET_USER_POST_FILTER_BY_ID]:(state) => (payload) =>{
    return state.userPosts.filter((f, i, arr) => f.userId == payload);
  }
};

store.mutations = {
  [SET_USERS](state,payload){
    return state.users=payload;
  },
  [SET_USER_POSTS](state,payload){
    if (payload && payload.id !== undefined) {
      const lastId = state.users.length > 0 ? state.users[state.users.length - 1].id : 0;
      const modifiedPayload = { ...payload };
      modifiedPayload.id = lastId + 1;
      state.users.push(modifiedPayload);
      return state;
    }
  },
  [SET_FETCH_USERS_POSTS](state,payload){
    return state.userPosts = payload;
  },
  [SET_DELETE_USER](state,payload){
    const indexToRemove = state.users.findIndex(item => item.id === payload);

    if (indexToRemove !== -1) {
      state.users.splice(indexToRemove, 1);
    }  
  }
  
};

store.actions = {
  [FETCH_ALL_USERS]({commit},payload){
    return new Promise((resolve, reject) => {
      network.users(payload).then(
        (resp) => {
          commit(SET_USERS,resp.data)
          resolve(resp);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },
  [FETCH_USER_POSTS]({commit},payload){
    return new Promise((resolve, reject) => {
      network.userPosts(payload).then(
        (resp) => {
          commit(SET_FETCH_USERS_POSTS,resp.data)
          resolve(resp);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  [POST_USER]({commit},payload){
    return new Promise((resolve, reject) => {
      network.postUser(payload).then(
        (resp) => {
          if(payload?.name!=''&&(payload?.company?.name!='')&&(payload?.email!='') && (payload?.userName!='')&&(payload?.website!='')){
            commit(SET_USER_POSTS,payload);          
            resolve(resp.data);
          }
        },
        (error) => {
          reject(error);
        }
      );
    });
  },
  [DELETE_USER]({commit},payload){
    commit(SET_DELETE_USER,payload);
  }


};

export default store;
