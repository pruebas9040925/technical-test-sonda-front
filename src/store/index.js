import { createStore } from "vuex";
import user from "../store/user";

export default createStore({
  state: {},
  getters: {},
  mutations: {},
  action: {},
  modules: {
    user,
  },
});
