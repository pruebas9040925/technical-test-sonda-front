import { createRouter, createWebHistory } from "vue-router";
import {
  Missing,
  Users,
  TableData,
  Home
} from "../views";
import cookie from "../utils/helpers/cookie.handler";
import store from "../store";

const defaultTitle = "technical Test";

const routes = [
  {
    path: "/:pathMatch(.*)",
    component: Missing,
    name: "Missing",
    meta: {
      requires_auth: false,
    },
  },

  {
    path: "/",
    name: "users",
    component: Users,
    meta: {
      requires_auth: false,
      title: defaultTitle,
    },
  },
  {
    path: "/inicio",
    name: "Home",
    component: Home,
    meta: {
      requires_auth: false,
      title: defaultTitle,
    },
  },
  
  {
    path: "/tablaAdmin",
    name: "table",
    component: TableData,
    meta: {
      requires_auth: true,
      title: defaultTitle,
    },
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
});


export default router;
