import VueCookies from "vue-cookies";
import moment from "moment";
import { createApp } from "vue";

createApp().use(VueCookies);

const cookieHandler = () => ({
  setTokens: (payload) => {
    const expires = moment().add(1, "y").toDate();
    VueCookies.set("accessToken", payload.token, {
      path: "/",
      sameSite: true,
      expires,
    });
    VueCookies.set("refreshToken", payload.refreshToken, {
      path: "/",
      sameSite: true,
      expires,
    });
  },
  getAccessToken: () => VueCookies.get("accessToken"),
  getRefreshToken: () => VueCookies.get("refreshToken"),
  removeToken: () => {
    VueCookies.remove("accessToken", { path: "/" });
    VueCookies.remove("refreshToken", { path: "/" });
  },
});

export default cookieHandler;
