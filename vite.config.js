import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import Components from 'unplugin-vue-components/vite';

export default defineConfig({
  
  plugins: [
    vue(),
    Components({
      resolvers: [],
    }),
  ],
  server: {
    host: '0.0.0.0',
    port: process.env.PORT||'3000',
    proxy: {
      '/json': {
        target: 'https://jsonplaceholder.typicode.com/',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/json/, ''),
      },
      '/api': {
        target: 'https://technicaltestsonda-291c1c8584be.herokuapp.com',
        changeOrigin: true,
        secure: true,
        headers: {
          'Access-Control-Allow-Origin': 'https://technicaltestsonda-291c1c8584be.herokuapp.com',
          'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE',
          'Access-Control-Allow-Headers': 'Content-Type, Authorization',
          'Access-Control-Allow-Credentials': 'true',
        },
        rewrite: (path) => path.replace(/^\/api/, ''),
      }
    }, 
  },
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
  optimizeDeps: {
    include: ['@vue/compiler-sfc']
  }
});




